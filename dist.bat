@echo off
rmdir /S /Q build > NUL
xcopy.exe src build /S /H /C /Y /I > NUL
del build\icons\*.psd
rmdir /S /Q dist > NUL
mkdir dist > NUL
powershell.exe -ExecutionPolicy Bypass -NoLogo -NoProfile -Command "$ProgressPreference = 'SilentlyContinue'; Compress-Archive -Force -Path .\build\* -DestinationPath .\dist\bing-to-whatever-chrome-X.XX.zip -CompressionLevel NoCompression; $ProgressPreference = 'Continue'"
copy dist\bing-to-whatever-chrome-X.XX.zip dist\bing-to-whatever-firefox-X.XX.xpi > NUL
