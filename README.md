# bing-to-whatever
This extension redirects Bing’s searches to whatever. Combined with Edge deflector (https://github.com/da2x/EdgeDeflector) lets you search through Cortana toolbar with whatever but Bing.

Available here [Firefox Add-ons](https://addons.mozilla.org/firefox/addon/bing-to-whatever/) | [Chrome Web Store](https://chrome.google.com/webstore/detail/aijoeagafmfpokknojmlfnjenlpefdpj/).

![gitmoji Screenshot](bing-to-whatever-screenshot.png)

## Build
Simply clone the repo, ``cd`` in then run ``sh ./dist.sh``.
*Dependencies : you'll need the `zip` command installed in your system.*