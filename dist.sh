#!/usr/bin/env sh
rm -R build 2>/dev/null
mkdir "build"
cp -R ./src/*.* ./build/
mkdir ./build/icons
cp -R ./src/icons/*.* ./build/icons/
cd ./build/
rm ./*.psd
zip -r ../build/build.zip ./
cd ../
rm -R dist 2>/dev/null
mkdir "dist"
cp ./build/build.zip ./dist/bing-to-whatever-chrome-X.XX.zip
mv ./build/build.zip ./dist/bing-to-whatever-firefox-X.XX.xpi
